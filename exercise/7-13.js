//es5
console.log(parseInt("11111111", 2));       //255
console.log(parseInt("0377", 8));           //255
console.log(0377 === 255); //嚴格模式下無法使用      //true  
//es6
console.log(0b11111111);    //二進位       //255
console.log(0o377);         //八進位       //255
console.log(0xFF);          //十六進位      //255