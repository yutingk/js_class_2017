function CreatePerson(name, gender) { //構造物件的函式(工廠方式)
    //原料
    //系統內部自動會執行，所以不需要寫
    //var this = new Object();
    //加工 
    this.name = name;
    this.gender = gender;
    this.showName = function() {
        console.log("my name:" + this.name);
    };
    this.showGender = function() {
        console.log("my gender:" + this.gender);
    };
    //出廠
    //系統內部自動會執行，所以不需要寫
    // return this;
}
var p1 = new CreatePerson("Mark", "male");
p1.showName();
p1.showGender();
var p2 = new CreatePerson("Susan", "female");
p2.showName();
p2.showGender();