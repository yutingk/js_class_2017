var json = {
    name: "John",
    show: function() {
        console.log(this.name);
    },
    empty: undefined,
    reg:/a/,
    nothing: null
};
var jsonString = JSON.stringify(json);
console.log(jsonString);
var jsonObject = JSON.parse(jsonString);

// function(), undefined, 
// Regular Expression(正規表達式)
// 在轉換成JSON格式時會被排除掉