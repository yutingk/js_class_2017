//es5
function link1(height, color, url) {
    var height = height || 50;
    var color = color || "red";
    var url = url || "http://www.abc.com";
    console.log(height, color, url);
}
link1(null, "Blue");                            // 50 "Blue" "http://www.abc.com"
link1(undefined, "", "http://www.xyz.com");     // 50 "red" "http://www.xyz.com"
//es6
function link2(height=50, color="red", url="http://www.abc.com") {
    console.log(height, color, url);
}
link2();                                        // 50 "red" "http://www.abc.com"
link2(null, " ",false);                         // null " " false
link2(undefined, "Blue");                       // 50 "Blue" "http://www.abc.com"
link2(undefined, "", "http://www.xyz.com");     // 50 "" "http://www.xyz.com"