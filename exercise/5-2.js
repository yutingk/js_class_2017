function base() {
    console.log(this, arguments);
}
base.call({name:"John"}, 2, 3); //第一個參數修改this,之後的參數為function參數
base.apply({name:"Mark"}, [2, 3]); //第一個參數修改this,第二個參數必須是陣列為function參數
base.bind({name:"Jack"})(2, 3); //第一個參數修改this,回傳一個新的function，可以動態改變this
console.log(typeof base.bind({name:"Sue"}));   //  回傳是一個function，並不會當下執行
base.bind({name:"Sue"})(2,3)    //等要執行時，須在後面() 並傳入參數

/**
 * 通常會用在
 * $(".btn").click(function(){
 *      ....
 * }).bind({})
 * 
 * // PS.被觸發時，不用再()，這種自動會有call back function會去執行
 * // .bind 的特點可以動態改變this
 */

// 第一個參數最好是傳入物件，就算傳入原始型別，也會被自動轉換成
// ex. 下例會被轉換為 Number {[[PrimitiveValue]]: 0} 
// this: 0 
base.call(0, 2, 3);
base.apply(0, [2, 3]);
