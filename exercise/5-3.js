function A() {
    this.abc = 12;
    this.run = function() {
        console.log("go");
    };
}
A.prototype.show = function() {
    console.log(this.abc);
};
function B() {
    A.call(this);
    /**
     * 用.call去繼承父類別(A)內的屬性與方法
     * 不過不會繼承到父類別(A)的原型方法
     */
}
var obj = new B();
console.log(obj.abc); //12
obj.run(); //"go"

var objA = new A();
objA.show(); //12

obj.show(); //obj.show is not a function

