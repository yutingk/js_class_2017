//共享參考
var arr1 = [1, 2, 3];
var arr2 = arr1;
arr2.push(4);
console.log(arr1); //預期 [1,2,3]   實際 [1,2,3,4]
console.log(arr2); //預期 [1,2,3,4] 實際 [1,2,3,4]

//避免共享參考
var arr1 = [1, 2, 3];
var arr2 = arr1.slice(); //slice會產生一個新的陣列
arr2.push(4);
console.log(arr1); //[1,2,3]
console.log(arr2); //[1,2,3,4]

// call by sharing
var arr1 = [1, 2, 3];
var arr2 = arr1;
arr2.push(4);
console.log(arr1); // [1,2,3,4]
arr2 = null;
console.log(arr1); // 預期 null   實際 [1,2,3,4]

var a={name:"Jack"};
var b=a;
b.name ="Tom";
console.log(a); // Object {name: "Tom"}
console.log(b); // Object {name: "Tom"}
b={};
console.log(a); // 預期 {}   實際 Object {name: "Tom"}
console.log(b); // {}
var c=b;
console.log(b); // {} 
console.log(c); // {} 
c=null;
console.log(b); // {} 
console.log(c); // null

/**
 * 其他傳值參考：
 * https://en.wikipedia.org/wiki/Evaluation_strategy
 * 不只call by value, call by reference, call by sharing
 * 這只是最常聽到的三種，還有很多其他的，可參考連結。
 */