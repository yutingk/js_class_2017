//es5
var xyz = ["x", "y", "z"];
var abc = ["a", "b", "c"];
console.log(abc.concat(xyz));   // (6) ["a", "b", "c", "x", "y", "z"]
//es6
var xyz = ["x", "y", "z"];
var abc = ["a", "b", "c",...xyz];
console.log(abc);               // (6) ["a", "b", "c", "x", "y", "z"]