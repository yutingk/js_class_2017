//es5
function() {
}
function(value) {
}
function(value, index) {
}
function(a) {
	return a*a;
}
//es6
() => { }

(value) => { }

(value, index) => { }

a => a*a //只有一個參數、只有一行回傳程式碼，可省略(), {}。

