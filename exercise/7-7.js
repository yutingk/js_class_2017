//es5
document.show1 = function () {
    console.log('this',this);  //#document
    var that = this;
    ["how", "are", "you"].forEach(function (value) {
        var h2 = document.createElement("h2");
        h2.innerText = value;
        console.log('this',this);  //Window
        console.log('that',that);  //#document
        that.body.appendChild(h2);
    });
};
document.show1();
//es6
document.show2 = function () {
    ["how", "are", "you"].forEach(value => {
        var h2 = document.createElement("h2");
        h2.innerText = value;
        this.body.appendChild(h2);
    })
};
document.show2();