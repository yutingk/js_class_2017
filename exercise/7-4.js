//es5
(function() {
    function foo() { return 1; }
    (function() {
        function foo() { return 2; }
        console.log(foo());         //2
    })();
    console.log(foo());             //1
})();
//es6
{
    function foo() { return 1; }
    {
        function foo() { return 2; }
        console.log(foo());         //2
    }
    console.log(foo());             //1
}


if(true){
    function foo(){
        return a;
    }
}else{
    function foo(){
        return b;
    }
}
console.log(foo());
// IE9-10: b, IE11:a, chrome:a, firefox:a
if(true)
    function foo(){return a;}
else
    function foo(){return b;}

console.log(foo());
// IE9-11: b, chrome:a, firefox:a
// IE11 只支援一半，if 沒有{}時，仍視為hoisting，回傳 b 