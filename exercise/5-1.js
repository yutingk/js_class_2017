function show(a, b) {
	console.log(this, a, b);
}
// 1. 直接呼叫
show(5,10);
// 2. 使用 new
new show(5,10);
// 3. 使用 call
show.call({}, 5, 10);