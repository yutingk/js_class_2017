var ary1 = new Array(12, 55, 43, 32, 44);
var ary2 = new Array(12, 33, 1);
Array.prototype.sum = function() {
    var output = 0;
    for (var i = 0; i < this.length; i++) {
        output += this[i];
    }
    return output;
};
console.log(ary1.sum());
console.log(ary2.sum());


// Array.prototype==ary1.__proto__   // true
// 原型prototype可以解決建立物件時，重複產生方法的問題