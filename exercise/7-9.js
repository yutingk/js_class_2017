//es5
function run1(a, b) {
    console.log(arguments);
    console.log(arguments.length);
    //console.log(arguments.filter((a)=>a>20));   // arguments.filter is not a function
}
run1(10, 20, 30);
//es6
function run2(a, ...params){
    console.log(a, params);
    console.log(params.length);
    console.log(params.filter((a)=>a>20));      // [30]
}
run2(10, 20, 30);