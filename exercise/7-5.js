//es5
var input = [10, 20, 30, 40, 50];
var output = input.map(function(value) {
    return value+1;
});
console.log(output);
//es6
var input = [10, 20, 30, 40, 50];
var output = input.map(value=> {
    return value+1;
});
console.log(output);

/**
 * function(){
 * }
 * 
 * () =>{
 * }
 * 當()裡面只有一個參數時，()可省略。
 * 當()裡面沒有參數或兩個以上參數時，不可省略。
 * 
 * 當{}裡面只有一行程式碼回傳時，{}可省略。
 */